package ru.t1.azarin.tm;

import static ru.t1.azarin.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showErrorCmd();
            return;
        }

        final String param = args[0];

        switch (param) {
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_HELP:
                showHelp();
                break;
            default:
                showErrorCmd();
        }
    }

    public static void showErrorCmd() {
        System.out.println("Wrong argument. Try again with another argument.");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Dmitry Azarin");
        System.out.println("azarindmitry1@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Display application version.\n", CMD_VERSION);
        System.out.printf("%s- Display developer info.\n", CMD_ABOUT);
        System.out.printf("%s - Show application command.\n", CMD_HELP);
    }

}
